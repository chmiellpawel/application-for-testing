package pl.project.jud.javaspringjpahsql.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.project.jud.javaspringjpahsql.domain.Origin;
import pl.project.jud.javaspringjpahsql.domain.Wine;

import javax.transaction.Transactional;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Transactional
public interface OriginService extends JpaRepository<Origin,Long> {

    <S extends Origin> S save(S Origin);

    Origin findOriginById(long id);

    Origin findOriginByContinent(@NotEmpty String continent);

    List<Origin> findOriginsByPartLike(@NotEmpty String part);

    @Transactional
    void delete(Origin origin);

    void deleteOriginByCountry(@NotEmpty String country);

    @Query(value="SELECT * FROM Origin ", nativeQuery = true)
    List<Origin> getAllOrigins();

    int countOriginPartLike(String name);

}
