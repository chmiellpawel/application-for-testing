package pl.project.jud.javaspringjpahsql.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.project.jud.javaspringjpahsql.domain.Tribe;

import javax.transaction.Transactional;
import javax.validation.constraints.NotEmpty;
import java.util.List;

public interface TribeService extends JpaRepository<Tribe,Long> {

    @Transactional
    <S extends Tribe> S save(S Tribe);

    Tribe findTribeById(long id);

    Tribe findTribeByClimate(@NotEmpty String climate);

    Tribe findTribeByDifficultyIsLikeAndGrowth(@NotEmpty String difficulty, @NotEmpty String growth);

    @Transactional
    void delete(Tribe tribe);

    void deleteAllByDifficulty(@NotEmpty String difficulty);

    @Query(value = "select * from Tribe", nativeQuery = true)
    List<Tribe> getAllTribes();
}
