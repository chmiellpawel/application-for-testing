package pl.project.jud.javaspringjpahsql.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.project.jud.javaspringjpahsql.domain.Wine;


import javax.transaction.Transactional;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.List;

@Transactional
public interface WineService extends JpaRepository<Wine,Long> {

    @Transactional
    <S extends Wine> S save(S Wine);

    Wine findWineById(long id);


    List<Wine> findWineByPriceGreaterThan(@NotEmpty double price);

    @Transactional
    void delete(Wine wine);


    @Query(value = "SELECT * FROM Wine", nativeQuery = true)
    List<Wine> getAllWines();

    List<Object> findWineWithPriceName(double price, double priceTwo);

    @Query(value = "SELECT w from Wine w where w.vegan = :vegan and w.name=:name")
    List<Wine> findWinesWithVeganAndName(@Param("vegan")boolean vegan,
                                         @Param("name")String name);

    @Query(value="select * from Wine w INNER JOIN Tribe t ON w.tribe_id = t.id where t.climate = 'temperate'", nativeQuery = true)
    List<Wine> findWinesWithTribeClimate();

    //between order desc
    List<Wine> findWineByPriceBetweenOrderByPriceDesc(@NotNull double price, @NotNull double price2);

    //like order asc
    List<Wine> findWineByCategoryLikeOrderByCategoryAsc(@NotEmpty String category);

    //distinct order desc
    List<Wine> findDistinctByKindOrderByIdDesc(@NotEmpty String kind);

    //count order asc
    int countWineByVeganOrderByPriceAsc(@NotNull boolean vegan);

    @Modifying
    @Transactional
    @Query("update Wine w set w.price = price * 0.5 where kind='Sweet'")
    void sale();







}
