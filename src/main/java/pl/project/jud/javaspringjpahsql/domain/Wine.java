package pl.project.jud.javaspringjpahsql.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@NamedQuery(name = "Wine.findWineWithPriceName",query = "SELECT w.id,w.price,w.name FROM Wine w where w.name LIKE '%Carolina%' AND w.price between :price AND :priceTwo")
public class Wine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String category;
    @NotEmpty
    private String kind;
    @NotNull
    private double price;
    @NotNull
    private boolean vegan;

    public Wine(){}

    public Wine(@NotEmpty String name, @NotEmpty String category, @NotEmpty String kind, @NotNull Double price, @NotNull boolean vegan) {
        this.name = name;
        this.category = category;
        this.kind = kind;
        this.price = price;
        this.vegan = vegan;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    Tribe tribe;
    @ManyToOne(cascade = CascadeType.ALL)
    Origin origin;

    public Tribe getTribe() {
        return tribe;
    }

    public void setTribe(Tribe tribe) {
        this.tribe = tribe;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Wine wine = (Wine) o;
        return id == wine.id &&
                Double.compare(wine.price, price) == 0 &&
                vegan == wine.vegan &&
                Objects.equals(name, wine.name) &&
                Objects.equals(category, wine.category) &&
                Objects.equals(kind, wine.kind);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, category, kind, price, vegan);
    }

    @Override
    public String toString() {
        return "Wine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", kind='" + kind + '\'' +
                ", price=" + price +
                ", vegan=" + vegan +
                '}';
    }
}
