package pl.project.jud.javaspringjpahsql.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Tribe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotEmpty
    private String growth;
    @NotEmpty
    private String difficulty;
    @NotEmpty
    private String climate;

    public Tribe() {}

    public Tribe(@NotEmpty String growth, @NotEmpty String difficulty, @NotEmpty String climate) {
        this.growth = growth;
        this.difficulty = difficulty;
        this.climate = climate;
        this.wines = new ArrayList<>();
    }

    @OneToMany(mappedBy = "tribe", cascade = CascadeType.ALL)
    private List<Wine> wines;

    public void addWine(Wine wine){
        wine.setTribe(this);
        wines.add(wine);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGrowth() {
        return growth;
    }

    public void setGrowth(String growth) {
        this.growth = growth;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    public String getClimate() {
        return climate;
    }

    public void setClimate(String climate) {
        this.climate = climate;
    }

    public List<Wine> getWines() {
        return wines;
    }

    public void setWines(List<Wine> wines) {
        this.wines = wines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tribe tribe = (Tribe) o;
        return id == tribe.id &&
                Objects.equals(growth, tribe.growth) &&
                Objects.equals(difficulty, tribe.difficulty) &&
                Objects.equals(climate, tribe.climate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, growth, difficulty, climate);
    }

    @Override
    public String toString() {
        return "Tribe{" +
                "id=" + id +
                ", growth='" + growth + '\'' +
                ", difficulty='" + difficulty + '\'' +
                ", climate='" + climate + '\'' +
                '}';
    }
}
