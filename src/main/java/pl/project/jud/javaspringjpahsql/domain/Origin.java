package pl.project.jud.javaspringjpahsql.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQuery(name="Origin.countOriginPartLike", query = "SELECT Count(o) from Origin o where part LIKE :name")
public class Origin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotEmpty
    private String continent;
    @NotEmpty
    private String country;
    @NotEmpty
    private String part;

    public Origin() {}

    public Origin(@NotEmpty String continent, @NotEmpty String country, @NotEmpty String part) {
        this.continent = continent;
        this.country = country;
        this.part = part;
        this.wines = new ArrayList<>();
    }

    @OneToMany(mappedBy = "origin",cascade = CascadeType.ALL)
    private
    List<Wine> wines;

    public void addWine(Wine wine){
        wine.setOrigin(this);
        wines.add(wine);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public List<Wine> getWines() {
        return wines;
    }

    public void setWines(List<Wine> wines) {
        this.wines = wines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Origin origin = (Origin) o;
        return id == origin.id &&
                Objects.equals(continent, origin.continent) &&
                Objects.equals(country, origin.country) &&
                Objects.equals(part, origin.part);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, continent, country, part);
    }

    @Override
    public String toString() {
        return "Origin{" +
                "id=" + id +
                ", continent='" + continent + '\'' +
                ", country='" + country + '\'' +
                ", part='" + part + '\'' +
                '}';
    }
}
