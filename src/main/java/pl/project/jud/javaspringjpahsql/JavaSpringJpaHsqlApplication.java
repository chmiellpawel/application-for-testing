package pl.project.jud.javaspringjpahsql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringJpaHsqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaSpringJpaHsqlApplication.class, args);
	}

}
