package pl.project.jud.javaspringjpahsql;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.project.jud.javaspringjpahsql.domain.Origin;
import pl.project.jud.javaspringjpahsql.domain.Tribe;
import pl.project.jud.javaspringjpahsql.domain.Wine;
import pl.project.jud.javaspringjpahsql.service.OriginService;
import pl.project.jud.javaspringjpahsql.service.TribeService;
import pl.project.jud.javaspringjpahsql.service.WineService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
class JavaSpringJpaHsqlApplicationTests {

	@Autowired
	WineService ws;
	@Autowired
	TribeService ts;
	@Autowired
	OriginService os;

	@BeforeEach
	void validManagers() {
		ws.deleteAll();
		ts.deleteAll();
		os.deleteAll();
	}


	@Test
	void insertFunctionTest(){
		//given
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Origin origin = new Origin("South America","Chile","North");
		Tribe tribe = new Tribe("long","easy","temperate");

		//then
		ws.save(wine);
		ws.save(wineTwo);
		os.save(origin);
		ts.save(tribe);

		//test
		Assertions.assertEquals(ws.findWineById(wine.getId()),wine);
		Assertions.assertEquals(ws.findWineById(wineTwo.getId()),wineTwo);
		Assertions.assertEquals(ws.getAllWines().size(),2);

		Assertions.assertEquals(os.findOriginById(origin.getId()),origin);
		Assertions.assertEquals(os.getAllOrigins().size(),1);

		Assertions.assertEquals(ts.findTribeById(tribe.getId()),tribe);
		Assertions.assertEquals(ts.getAllTribes().size(),1);
	}

	@Test
	void deteleFunctionTest(){
		//given
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Origin origin = new Origin("South America","Chile","North");
		Tribe tribe = new Tribe("long","easy","temperate");

		//then
		ws.save(wine);
		os.save(origin);
		ts.save(tribe);


		ws.delete(wine);
		os.delete(origin);
		ts.delete(tribe);
		//test
		Assertions.assertEquals(ws.getAllWines().size(),0);
		Assertions.assertEquals(os.getAllOrigins().size(),0);
		Assertions.assertEquals(ts.getAllTribes().size(),0);
	}

	@Test
	void updateFunctionTest(){

		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Origin origin = new Origin("South America","Chile","North");
		Tribe tribe = new Tribe("long","easy","temperate");

		ws.save(wine);
		os.save(origin);
		ts.save(tribe);

		Assertions.assertEquals(ws.findWineById(wine.getId()).getName(),"Fiuza 3");
		Assertions.assertEquals(ws.findWineById(wine.getId()).getCategory(),"Red");
		Assertions.assertEquals(os.findOriginById(origin.getId()).getCountry(),"Chile");
		Assertions.assertEquals(os.findOriginById(origin.getId()).getPart(),"North");
		Assertions.assertEquals(ts.findTribeById(tribe.getId()).getGrowth(),"long");
		Assertions.assertEquals(ts.findTribeById(tribe.getId()).getClimate(),"temperate");

		Wine tempWine = ws.findWineById(wine.getId());
		Origin tempOrigin = os.findOriginById(origin.getId());
		Tribe tempTrible = ts.findTribeById(tribe.getId());

		tempWine.setName("Updated name");
		tempWine.setCategory("Updated category");
		tempOrigin.setCountry("Updated country");
		tempOrigin.setPart("Updated part");
		tempTrible.setGrowth("Updated growth");
		tempTrible.setClimate("Updated climate");

		ws.save(tempWine);
		os.save(tempOrigin);
		ts.save(tempTrible);

		Assertions.assertEquals(ws.findWineById(wine.getId()).getName(),"Updated name");
		Assertions.assertEquals(ws.findWineById(wine.getId()).getCategory(),"Updated category");
		Assertions.assertEquals(os.findOriginById(origin.getId()).getCountry(),"Updated country");
		Assertions.assertEquals(os.findOriginById(origin.getId()).getPart(),"Updated part");
		Assertions.assertEquals(ts.findTribeById(tribe.getId()).getGrowth(),"Updated growth");
		Assertions.assertEquals(ts.findTribeById(tribe.getId()).getClimate(),"Updated climate");
	}

	@Test
	void selectFunctionTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Origin origin = new Origin("South America","Chile","North");
		Tribe tribe = new Tribe("long","easy","temperate");

		ws.save(wine);
		ws.save(wineTwo);
		os.save(origin);
		ts.save(tribe);

		Assertions.assertEquals(ws.getAllWines().size(),2);
		Assertions.assertEquals(ws.findWineById(wine.getId()),wine);
		Assertions.assertEquals(ws.findWineById(wineTwo.getId()),wineTwo);

		Assertions.assertEquals(os.getAllOrigins().size(),1);
		Assertions.assertEquals(os.findOriginById(origin.getId()),origin);

		Assertions.assertEquals(ts.getAllTribes().size(),1);
		Assertions.assertEquals(ts.findTribeById(tribe.getId()),tribe);


	}

	@Test
	void oneToManyConnectionBetweenOriginWineAndTribeWine(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Origin origin = new Origin("South America","Chile","North");
		Tribe tribe = new Tribe("long","easy","temperate");

		tribe.addWine(wine);
		tribe.addWine(wineTwo);

		origin.addWine(wine);
		origin.addWine(wineTwo);

		Assertions.assertEquals(wine.getTribe(),tribe);
		Assertions.assertEquals(wine.getOrigin(),origin);
		Assertions.assertEquals(wineTwo.getTribe(),tribe);
		Assertions.assertEquals(wineTwo.getOrigin(),origin);
	}

	@Test
	void manyToOneConnectionBetweenWineOriginAndWineTribe(){

		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Origin origin = new Origin("South America","Chile","North");
		Tribe tribe = new Tribe("long","easy","temperate");

		tribe.addWine(wine);
		tribe.addWine(wineTwo);

		origin.addWine(wine);
		origin.addWine(wineTwo);

		Assertions.assertEquals(tribe.getWines().size(),2);
		Assertions.assertEquals(tribe.getWines().get(0),wine);
		Assertions.assertEquals(tribe.getWines().get(1),wineTwo);

		Assertions.assertEquals(origin.getWines().size(),2);
		Assertions.assertEquals(origin.getWines().get(0),wine);
		Assertions.assertEquals(origin.getWines().get(1),wineTwo);


	}

	//NamedQueries
	@Test
	void findWineByPriceGreaterThanTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",25.99,false);

		List<Wine> wineTable = new ArrayList<>();
		wineTable.add(wineTwo);
		wineTable.add(wineThree);
		wineTable.add(wineFour);
		wineTable.add(wineFive);

		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);

		Assertions.assertEquals(ws.findWineByPriceGreaterThan(25.00).size(),4);
		Assertions.assertEquals(ws.findWineByPriceGreaterThan(25.00),wineTable);
	}

	@Test
	void findWineByPriceAndNameLikeTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",25.99,false);

		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);

		Assertions.assertEquals(ws.findWineWithPriceName(20.0,30.0).size(),2);
	}

	@Test
	void countOriginWithCountryAndPartLikeTest(){
		Origin origin = new Origin("South America","Chile","North");
		Origin originTwo = new Origin("Europe","Italy","Middle");
		Origin originThree = new Origin("Europe","France","South");

		os.save(origin);
		os.save(originTwo);
		os.save(originThree);

		Assertions.assertEquals(os.countOriginPartLike("%th"),2);
	}
	//@Query
	@Test
	void findWinesWithVeganAndNameTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",25.99,false);

		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);

		Assertions.assertEquals(ws.findWinesWithVeganAndName(true,"Santa Carolina Premio").get(0),wineFour);
		Assertions.assertEquals(ws.findWinesWithVeganAndName(false,"Prosecco").get(0),wineThree);

	}

	@Test
	void findWinesWithTribeClimateTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",25.99,false);
		Tribe tribe = new Tribe("long","easy","temperate");
		Tribe tribeTwo = new Tribe("short","difficult","hot");
		Tribe tribeThree = new Tribe("long","difficult","temperate");

		tribeThree.addWine(wine);
		tribeTwo.addWine(wineTwo);
		tribeTwo.addWine(wineThree);
		tribe.addWine(wineFour);
		tribeTwo.addWine(wineFive);


		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);
		ts.save(tribe);
		ts.save(tribeTwo);
		ts.save(tribeThree);

		Assertions.assertEquals(ws.findWinesWithTribeClimate().size(),2);
		Assertions.assertEquals(ws.findWinesWithTribeClimate().get(0),wine);
		Assertions.assertEquals(ws.findWinesWithTribeClimate().get(1),wineFour);


	}

	// Query Methods
	@Test
	void findWineByPriceBetweenOrderByPriceDescTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",26.00,false);

		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);

		Assertions.assertEquals(ws.findWineByPriceBetweenOrderByPriceDesc(20.0,30.0).size(),3);
		Assertions.assertEquals(ws.findWineByPriceBetweenOrderByPriceDesc(20.0,30.0).get(0), wineFive);
		Assertions.assertEquals(ws.findWineByPriceBetweenOrderByPriceDesc(20.0,30.0).get(1), wineFour);
		Assertions.assertEquals(ws.findWineByPriceBetweenOrderByPriceDesc(20.0,30.0).get(2), wine);
	}

	@Test
	void findWineByCategoryLikeOrderByCategoryAscTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",26.00,false);
		String like = "%ed";

		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);

		Assertions.assertEquals(ws.findWineByCategoryLikeOrderByCategoryAsc(like).size(),3);
		Assertions.assertEquals(ws.findWineByCategoryLikeOrderByCategoryAsc(like).get(0), wine);
		Assertions.assertEquals(ws.findWineByCategoryLikeOrderByCategoryAsc(like).get(1), wineTwo);
		Assertions.assertEquals(ws.findWineByCategoryLikeOrderByCategoryAsc(like).get(2), wineFive);
	}

	@Test
	void findDistinctByKindOrderByIdDescTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",26.00,false);

		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);

		Assertions.assertEquals(ws.findDistinctByKindOrderByIdDesc("Dry").size(),1);
		Assertions.assertEquals(ws.findDistinctByKindOrderByIdDesc("Dry").get(0),wine);

	}

	@Test
	void countWineByVeganOrderByPriceAsc(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineTwo = new Wine("Sachino","Red","Half-Dry",40.00,true);
		Wine wineThree = new Wine("Prosecco","White","Half-Dry",35.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",25.99,true);
		Wine wineFive = new Wine("Santa Carolina Premio","Red","Half-Sweet",26.00,false);

		ws.save(wine);
		ws.save(wineTwo);
		ws.save(wineThree);
		ws.save(wineFour);
		ws.save(wineFive);

		Assertions.assertEquals(ws.countWineByVeganOrderByPriceAsc(true),2);
		Assertions.assertEquals(ws.countWineByVeganOrderByPriceAsc(false),3);

	}

	@Test
	void saleTest(){
		Wine wine = new Wine("Fiuza 3","Red","Dry",20.00,false);
		Wine wineFour = new Wine("Santa Carolina Premio","White","Sweet",26.00,true);


		ws.save(wine);
		ws.save(wineFour);
		ws.sale();
		List<Wine> wines  = ws.getAllWines();

		Assertions.assertEquals(wines.get(0).getPrice(),wine.getPrice(),0.0001);
		Assertions.assertEquals(wines.get(1).getPrice(),wineFour.getPrice()*0.5,0.0001);

	}



}
